data "azurerm_client_config" "current" {}

# ================ Test Cosmos DB account ================ #
resource "azurerm_cosmosdb_account" "cosmos" {
  name                = "${var.userName}-test-cosmos-db"
  location            = var.location
  resource_group_name = var.rgName
  offer_type          = "Standard"
  kind                = "GlobalDocumentDB"

  enable_automatic_failover = true

  consistency_policy {
    consistency_level       = "BoundedStaleness"
    max_interval_in_seconds = 300
    max_staleness_prefix    = 100000
  }

  geo_location {
    location          = var.location
    failover_priority = 0
  }
}

resource "azurerm_cosmosdb_sql_database" "example" {
  name                = var.dbName
  resource_group_name = azurerm_cosmosdb_account.cosmos.resource_group_name
  account_name        = azurerm_cosmosdb_account.cosmos.name
  throughput          = 400
}

resource "azurerm_cosmosdb_sql_container" "example" {
  name                  = "order"
  resource_group_name   = azurerm_cosmosdb_account.cosmos.resource_group_name
  account_name          = azurerm_cosmosdb_account.cosmos.name
  database_name         = azurerm_cosmosdb_sql_database.example.name
  partition_key_path    = "/locationId"
  partition_key_version = 1
  throughput            = 400

  indexing_policy {
    indexing_mode = "consistent"

    included_path {
      path = "/*"
    }

    included_path {
      path = "/included/?"
    }

    excluded_path {
      path = "/_etag/?"
    }
  }

#   unique_key {
#     paths = ["/definition/idlong", "/definition/idshort"]
#   }
}

# ================ Roles ================ #
resource "azurerm_cosmosdb_sql_role_definition" "contributor" {
  resource_group_name = var.rgName
  account_name        = azurerm_cosmosdb_account.cosmos.name
  name                = "testcontributor"
  assignable_scopes = [
    "/subscriptions/${data.azurerm_client_config.current.subscription_id}/resourceGroups/${var.rgName}/providers/Microsoft.DocumentDB/databaseAccounts/${azurerm_cosmosdb_account.cosmos.name}",
    "/subscriptions/${data.azurerm_client_config.current.subscription_id}/resourceGroups/${var.rgName}/providers/Microsoft.DocumentDB/databaseAccounts/${azurerm_cosmosdb_account.cosmos.name}/dbs/${var.dbName}"
  ]

  permissions {
    data_actions = [
      "Microsoft.DocumentDB/databaseAccounts/readMetadata",
      "Microsoft.DocumentDB/databaseAccounts/sqlDatabases/containers/*",
      "Microsoft.DocumentDB/databaseAccounts/sqlDatabases/containers/items/*"
    ]
  }
}

resource "azurerm_cosmosdb_sql_role_assignment" "example" {
  resource_group_name = var.rgName
  account_name        = azurerm_cosmosdb_account.cosmos.name
  role_definition_id  = azurerm_cosmosdb_sql_role_definition.contributor.id
  principal_id        = var.aksAzwiSPNObjId
  scope               = "/subscriptions/${data.azurerm_client_config.current.subscription_id}/resourceGroups/${var.rgName}/providers/Microsoft.DocumentDB/databaseAccounts/${azurerm_cosmosdb_account.cosmos.name}"
}
