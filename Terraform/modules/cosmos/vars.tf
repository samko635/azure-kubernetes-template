variable "baseTags" {
  description = "Servian base resource tags"
  type        = map(string)
}

variable "userName" {
  type        = string
  description = "username used for resource prefix"
}

variable "env" {
  type        = string
  description = "Environment name"
}

variable "location" {
  type        = string
  description = "Location"
}

variable "rgName" {
  type        = string
  description = "RG name"
}

variable "dbName" {
  type = string
  description = "(optional) Cosmos DB name"
  default = "testdb"
}

variable "aksAzwiSPNObjId" {
  type        = string
  description = "Azure Workload Identity SPN object ID"
}
