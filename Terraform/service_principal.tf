# # Only work if DevOps agent SPN has "Appplication.ReadWrite.All" and "Directory.ReadWrite.All"
# # ================ Azure AD Application and SPN ================ #
# resource "azuread_application" "directory_role_app" {
#   display_name = "aks-workload-sample-app"
#   owners       = [data.azuread_client_config.current.object_id]
# }

# resource "azuread_service_principal" "directory_role_app" {
#   application_id = azuread_application.directory_role_app.application_id
#   use_existing   = true
# }

# resource "azuread_service_principal_password" "directory_role_app" {
#   service_principal_id = azuread_service_principal.directory_role_app.id
# }

# # Federated credentials for AKS application pods (4-workload-identity)
# resource "azuread_application_federated_identity_credential" "aks_app_1" {
#   application_object_id = azuread_application.directory_role_app.object_id
#   display_name          = "kubernetes-federated-credential"
#   description           = "Kubernetes service account federated credential"
#   audiences             = ["api://AzureADTokenExchange"]
#   subject               = "system:serviceaccount:${var.aksNamespace}:${var.aksServiceAccountName}"
#   issuer                = module.aks.oidc_issuer_url
# }

# # Federated credentials for AKS KEDA operator
# resource "azuread_application_federated_identity_credential" "aks_keda_operator" {
#   application_object_id = azuread_application.directory_role_app.object_id
#   display_name          = "keda-operator-federated-credential"
#   description           = "KEDA operator service account federated credential"
#   audiences             = ["api://AzureADTokenExchange"]
#   subject               = "system:serviceaccount:keda:keda-operator"
#   issuer                = module.aks.oidc_issuer_url
# }

# resource "azuread_group" "aks_admins" {
#   display_name     = "aksAdminGroup"
#   security_enabled = true
#   owners           = [data.azuread_client_config.current.object_id]

#   members = [
#     data.azuread_client_config.current.object_id,
#     azuread_service_principal.directory_role_app.object_id,
#   ]
# }
