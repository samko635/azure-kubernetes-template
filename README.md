# Getting started
This is a template repo that's been built using Gitlab, Azure, Terraform, ArgoCD, Helm.

## Prerequisites
- Gitlab repo
- AD Application & SPN for Gitlab devops agent
- Terraform state remote storage (e.g. Azure storage account)
- Kubectl and AZ CLI installed in the local machine

## Gitlab CI
### CI requirements
- Docker image with az cli & terraform cli for infrastructure deploy (e.g. zenika/terraform-azure-cli)
- Set CI/CD environment variables to integrate with Azure (ARM_SUBSCRIPTION_ID, ARM_TENANT_ID, ARM_CLIENT_ID, ARM_CLIENT_SECRET, ARM_ACCESS_KEY)

## Kubectl CLI (Local setting)
```bash
# Run this to add AKS cluster to kubectl config
az aks get-credentials --resource-group ${ResourceGroupName} --name ${ClusterName} --admin

# Switch context if it's already been added
kubectl config view
kubectl config use-context ${ContextName}
```

## Argo CD (Local setting)
Installing Argo CD to the AKS cluster will be done via Terraform helm_release. Or you could follow [this ArgoCD official guide](https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd) to install Argo CD on your K8s cluster.

### 1. Access ArgoCD dashboard
Gain the default admin secret (MUST change afterwards)
```bash
# Get the service IP (Check EXTERNAL-IP for argocd-server)
kubectl get svc -n argocd

# Get login secret (user name is 'admin' by default)
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
```
### 2. Create Argo CD application
Once an application is created and connected to the repo, ArgoCD will automatically deploy the changes so that the application status syncs with the manifest files in the repo. This is an one-off activity.
```bash
# Run this once to synchronize Argo CD agent with our git repo
kubectl apply -n argocd -f application.yaml
```

## Install KEDA
Installing KEDA to the AKS cluster will be done via Terraform helm_release. Or you could simply enable KEDA add-on feature but it [only allows 2.7.0 version](https://docs.microsoft.com/en-us/azure/aks/keda-deploy-add-on-cli) as of Sept 20th, 2022. (No support for Azure Workload identity)
Follow [this guide](https://keda.sh/docs/2.8/deploy/) for installing KEDA in Kubernetes cluster, or [this guide](https://github.com/kedacore/sample-dotnet-worker-servicebus-queue/blob/main/workload-identity.md) for installing KEDA WITH Workload identity.

## Workload identity
Installing Workload identity to the AKS cluster will be done via Terraform helm_release. Or you could follow this [quick & Easy official tutorial with CLI](https://azure.github.io/azure-workload-identity/docs/quick-start.html).
